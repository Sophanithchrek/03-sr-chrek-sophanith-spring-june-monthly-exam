package com.examination.springjuneexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJuneExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJuneExamApplication.class, args);
    }

}
